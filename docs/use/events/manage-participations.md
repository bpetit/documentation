# Manage event participations

## List participations

As the event creator you can see who particpate to an event. To do so you have to click either:

  * link **X persons going** on your event page:

      ![number of participations - event view](../../images/en/event-participations-number-event-page-EN.png)

  * or **My events** on top bar menu and **Manage participations** link:

      ![event actions image](../../images/en/events-page-manage-participations-EN.png)

![event participations list](../../images/en/event-participations-list-messages-EN.png)

## Reject a participation

![gif reject participation actions](../../images/en/event-participations-list-messages-reject-EN.gif)

To reject participations you have to [go to participations list](#list-participations) and:

  1. check participations boxes you want to reject
  * click **Reject participant** button

Once done, **Rejected** label will be displayed:

![image rejected particpant](../../images/en/event-participations-list-messages-rejected-EN.png)

!!! info
    Participant will receive a notification of this rejection by email.

## Approve participant

When **I want to approve every participation request** option is checked by event organiser, participants see a modal where they optionally can add a short text and have to click **Confirm my participation** button:

![confirmation participation modal](../../images/en/event-participation-confirmation.png)

Organiser can approve participation by [going to participations list](#list-participations) and:

  1. clicking checkbox(es) in front of participation(s)
  * clicking **Approve participant**

![image participation approval](../../images/en/event-participation-approval.png)

Once done, **Participant** label will be displayed:

![image label paricipant](../../images/en/event-participation-approved.png)
