# Particpate in an event

## With an account

When you want to participate in an event, you have to:

  1. dipslay event page (it's easier this way ;))
  * click **Participate** button below the event banner
  * select which profile you want to use for this event

![event participation with account img](../../images/en/event-tree-participation-rose-utopia-EN.jpg)

That's it! **Participate** button is now **I participe**.

!!! note
    If you click **Participate** button without being logged in you will be prompted to do so.

!!! info
    If participations approval is activated you have to wait until an organizer approve your participation.

## Anonymously

!!! note
    Organizer has to **allow** [anonymous participations](../../events/create-events/#who-can-view-this-event-and-participate). By default, their are not.

To particpate in an event anonymously, you have to:

  1. display event page (it's easier this way ;))
  * click **Participate** button below the event banner
  * click **I don't have a Mobilizon account** to participate using you email address
  * enter your email address in **Email** field
  * [Optional] write a message to the organizer(s)
  * click **Send email**

![anonymous participation img](../../images/en/rose-utopia-anonymous-participation-web-EN.png)

An email will be send to you to confirm your email address. Click **Confirm my e-mail address**. Congratulations: your participation has been validated!

### Remember my participation in this browser

**Optional**: You can check **Remember my participation in this browser**: it will allow to display and manage your participation status on the event page when using this device. Uncheck if you're using a public device.

If checked, you can cancel your anonymous participation by click **Cancel anonymous participation** button below the event banner.

!!! info
    If participations approval is activated you have to wait until an organizer approve your participation.
