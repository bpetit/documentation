# Use Mobilizon

---

## How to use Mobilizon?

First off, thank you for using Mobilizon!
This documentation aims to help you through the different options and features.

### User

  * [Create an account](users/account-creation.md)
  * [Setup your account](users/account-settings.md)
  * [Create an new profile](users/profile-creation.md)
  * [Search](users/search.md)

### Events

  * [Comment an event](events/comment-event.md)
  * [Create an event](events/create-events.md)
  * [Manage event participations](events/manage-participations.md)
  * [Participate in an event](events/participate-event.md)
  * [Share, report, edit an Event](events/event-action.md)

### Groups

  * [Create and edit a group](groups/create-group.md)
  * [Invitations](groups/invite-group.md)
  * [Manage roles](groups/roles-group.md)
  * [Group discussions](groups/discussion-group.md)
  * [Group pages](groups/group-page.md)
  * [Group post](groups/group-post.md)
  * [Group ressources](groups/group-ressources.md)

### Administration & moderation

  * [How to deal with moderation](administration/moderation.md)
