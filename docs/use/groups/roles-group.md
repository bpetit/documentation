# Manage roles

## Roles

| ACTIONS                                | ADMINISTRATOR   | MODERATOR       | MEMBER          |
| :--------------------------------------|:---------------:|:---------------:|:---------------:|
| Start / edit / delete a discussion     | ✔               | ✔               | ✔ *               |
| Add / edit / delete a resource         | ✔               | ✔               | ✔               |
| Create / delete an event               | ✔               | ✔               | ❌              |
| Post / edit / delete a public message  | ✔               | ✔               | ❌              |
| Add members                            | ✔               | ❌              | ❌              |
| Manage roles                           | ✔               | ❌              | ❌              |
|                                        |                 |                 |                 |

\* who created the discussion


![](../../images/en/group-members-list-EN.png)

When a person is invited and accept this invitation to join your group, she is, by default, *only* **member**.

## Promote

You can promote an account to **moderator** or **administrator** by:

  1. clicking **My groups** button on top bar menu
  * clicking the group you want to manage
  * clicking **Add / Remove...** link in your group banner
  * clicking **Promote** button in front of the user you want to promote.

!!! info
    You only can promote **one level at the time**: **member** then **moderator** then **administrator**

## Demote

You can demote an account to **administrator** by:

  1. clicking **My groups** button on top bar menu
  * clicking the group you want to manage
  * clicking **Add / Remove...** link in your group banner
  * clicking **Demote** button in front of the user you want to demote.

!!! info
    You only can demote **one level at the time**: **administrator** then **moderator** then **member**

## Remove

To remove a member you have to:

1. clicking **My groups** button on top bar menu
* clicking the group you want to manage
* clicking **Add / Remove...** link in your group banner
* click **Remove** button in front of the user

!!! note
    If the user is a moderator or an administrator, you have to [demote](#demote) them first.
