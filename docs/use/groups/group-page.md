# Group page

## Join a group

If the group is [configured to allow anyone to join the group without invitation](../../groups/create-group/#settings), you must click the **Join Group** button on the group page. If you are not logged in to your account, you will be prompted to do so.

## Report a group

To report a group, you have to:

  1. click ⋅⋅⋅ button
  * click **Report** button:

    ![report group image](../../images/en/report-group.png)

  * [Optional but **recommended**] filling the report modal with a comment:
    ![groupe report modal image](../../images/en/report-group-modal.png)
