# Invitations

## Invite someone to join a group

To invite accounts to your group, you have to:

  1. click **My groups** button on top bar menu
  * click the one you want to invite people on the list
  * click **Add / Remove…** link in your group banner
  * enter the new member ID in **Invite a new member** field
  * click **Invite member** button

Invited account will receive an email notification if [notifications are activated](../../users/account-settings/#email-notifications).

!!! note
    By default, invited account's role is **member**.


## Being invited to join a group

When someone invites you to join a group (see above), you receive an email notification.

From **My groups** page, you either **accept** or **decline** this invitation.

!!! note
    By default your role is **member**.

![goup invitation](../../images/en/group-accept-invitation-EN.png)
