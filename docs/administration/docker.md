# Docker

The docker-compose provides an app container for Mobilizon itself and a database container for PostgreSQL (Postgis). You'll need your own reverse-proxy to handle TLS termination (on port 4000).

!!! danger "Limitations"
    The docker image currently supports a minimal basic set of options required to run mobilizon. For now, you won't be able to handle [any configuration](./configure/index.md) related to emails, geocoders, 3rd-party auth,…

## Get the docker-compose repo

```bash
git clone https://framagit.org/framasoft/joinmobilizon/docker.git docker-mobilizon
cd docker-mobilizon
```

## Update the env file

```bash
cp env.template .env
```

Edit the `.env` content with your own settings.
More settings can be added in .env, see all in docker-compose.yml file.

You can generate values for `MOBILIZON_INSTANCE_SECRET_KEY_BASE` and `MOBILIZON_INSTANCE_SECRET_KEY` with:

```bash
gpg --gen-random --armor 1 50
```

## Run the service

Start both the database and the Mobilizon service. The Mobilizon service will wait for the database one.

```bash
docker-compose up -d
```

A migration will be automatically run before starting Mobilizon (can be run even if no migration is needed without incidence).

## Run a mobilizon_ctl command

Any task documented in [CLI tasks](./CLI tasks/index.md) can be executed on Docker as well, but it need to be adapted, as the `mix` tool is not available in releases which are used in Docker images. Instead, we provide the `mobilizon_ctl` wrapper tool that can call any Mobilizon `mix` task (namedspaced under `mobilizon.`).

```bash
docker-compose exec mobilizon mobilizon_ctl [options]
```

For instance, creating a new admin without Docker would be something like:
```bash
MIX_ENV=prod mix mobilizon.users.new "your@email.com" --admin --password "Y0urP4ssw0rd"
```

Inside Docker, it would be something like:
```bash
docker-compose exec mobilizon mobilizon_ctl users.new "your@email.com" --admin --password "Y0urP4ssw0rd"
```

## Update the service

Pull the latest image, then update the service. The migrations are automatically performed:

```bash
docker-compose pull framasoft/mobilizon
docker-compose up -d
```
