# Media tasks

## Clean orphan media files

*Introduced in Mobilizon 1.0.3*

Manually run the cleaning of orphan media files.

```bash
mix mobilizon.media.clean_orphan [<options>]
```

### Options
* `-v`, `--verbose` List the files that were deleted. Implied when using `--dry-run`.
* `-d`, `--days` The number of days after which a media file is considered an orphan
* `--dry-run` Run the task, but don't delete media files, just list them. Implies `--verbose`.