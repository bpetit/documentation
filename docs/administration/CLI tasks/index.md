# CLI Tasks

Mobilizon provides commands to perform some tasks.

!!! tip "Environment"
    You need to run these commands with the appropriate environment loaded, so probably prefix with `MIX_ENV=prod`.


The commands are documented as if they were being used a source installation:
```bash
MIX_ENV=prod mix mobilizon.command.subcommand
```

If you use Elixir releases (such as [Docker](../docker.md)), the commands need to be changed like this:
```bash
mobilizon_ctl command.subcommand
```

For instance, creating a new admin without Docker would be something like:
```bash
MIX_ENV=prod mix mobilizon.users.new "your@email.com" --admin --password "Y0urP4ssw0rd"
```

Inside Docker, it would be something like:
```bash
docker-compose exec mobilizon mobilizon_ctl users.new "your@email.com" --admin --password "Y0urP4ssw0rd"
```